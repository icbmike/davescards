package com.example.davescards;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RedditViewCreator implements ViewCreator {

	@Override
	public View createView(Context context) {
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    LinearLayout card = (LinearLayout) inflater.inflate(R.layout.reddit_card, null);
	    
	    ImageView logo = (ImageView) card.findViewById(R.id.logo);
	    
	    logo.setImageResource(R.drawable.reddit_alien);
		TextView title = (TextView)card.findViewById(R.id.title);
		title.setText("Lolol Repost");
		
		ImageView pic = (ImageView) card.findViewById(R.id.redditPic);
		pic.setImageResource(R.drawable.dave);
		
		return card;
	}

}
