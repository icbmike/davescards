package com.example.davescards;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

	private int cardCount = 0;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}
	
	
	public boolean onAddNew(MenuItem item){
		
		ViewCreator vc;
		if(cardCount % 3 == 0 || cardCount % 5 == 0){
			vc = new RedditViewCreator();
		}else{
			vc = new FacebookViewCreator();
		}
		cardCount++;
		
		View card = vc.createView(this);
		
		LinearLayout cardContainer = (LinearLayout) findViewById(R.id.cardContainer);
		cardContainer.addView(card);
		
		
		return true;
	}

}
