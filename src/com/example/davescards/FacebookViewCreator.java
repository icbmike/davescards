package com.example.davescards;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FacebookViewCreator implements ViewCreator {

	private Context c;
	
	@Override
	public View createView(Context context) {
		c = context;
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    LinearLayout card = (LinearLayout) inflater.inflate(R.layout.facebook_card, null);
	    
	    //Construct the header
	    ImageView logo = (ImageView) card.findViewById(R.id.logo);
	    logo.setImageResource(R.drawable.facebook);
		TextView title = (TextView)card.findViewById(R.id.title);
		title.setText("Mike Liked Your status");
		
		//Implement the button
		ImageButton ib = (ImageButton) card.findViewById(R.id.facebookButton);
		ib.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Toast.makeText(FacebookViewCreator.this.c, "FACEBOOK WHAT", Toast.LENGTH_LONG).show();
				
			}
		});
		ib.setImageResource(R.drawable.like);
		return card;
	}

}
