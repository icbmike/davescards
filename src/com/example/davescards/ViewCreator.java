package com.example.davescards;

import android.content.Context;
import android.view.View;

public interface ViewCreator {

	public View createView(Context context);
}
